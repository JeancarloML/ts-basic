(() => {
  const products: Product[] = [];

  type Product = {
    title: string;
    createAt: Date;
    stock: number;
    size?: string;
  };
  const addProduct = (data: Product) => {
    products.push(data);
  };

  addProduct({
    title: 'Producto 1',
    createAt: new Date(),
    stock: 12,
  });
  addProduct({
    title: 'Producto 2',
    createAt: new Date(),
    stock: 124,
    size: 'M',
  });

  console.log('🚀 ~ file: 14-functions-objs.ts ~ line 33 ~ products', products);
})();
