(() => {
  let productTitle = 'My amazing product';
  /* productTitle = null;
  productTitle = () => {};
  productTitle = 123; */
  productTitle = 'My amazing product 2';
  console.log('🚀 ~ file: 06-strings.ts ~ line 7 ~ productTitle', productTitle);

  const productDescription = 'balalblalballblablalb1';
  console.log(
    '🚀 ~ file: 06-strings.ts ~ line 10 ~ productDescription',
    productDescription
  );

  const summary = `
    title: ${productTitle}
    description: ${productDescription}
  `;
  console.log('🚀 ~ file: 06-strings.ts ~ line 16 ~ summary', summary);


  const myString: string = 'Hello';
})();
