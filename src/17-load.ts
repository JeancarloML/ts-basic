import _ from 'lodash';

const data = [
  {
    username: 'moe',
    role: 'admin',
  },
  {
    username: 'larry',
    role: 'admin',
  },
  {
    username: 'curly',
    role: 'user',
  },
  {
    username: 'shep',
    role: 'user',
  },
];

let rta = _.groupBy(data, (item) => item.role);
console.log("🚀 ~ file: 17-load.ts ~ line 23 ~ rta", rta)

