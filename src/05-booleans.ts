(() => {
  let isEnable = true;
  isEnable = false;

  let isNew: boolean = false;
  console.log('🚀 ~ file: 05-booleans.ts ~ line 6 ~ isNew', isNew);
  isNew = true;
  console.log('🚀 ~ file: 05-booleans.ts ~ line 8 ~ isNew', isNew);

  const random = Math.random();
  console.log('🚀 ~ file: 05-booleans.ts ~ line 11 ~ random', random);

  isNew = random > 0.5 ? true : false;
  console.log('🚀 ~ file: 05-booleans.ts ~ line 14 ~ isNew', isNew);

  const myBoolean: boolean = true;
  console.log('🚀 ~ file: 05-booleans.ts ~ line 17 ~ myBoolean', myBoolean);
})();
