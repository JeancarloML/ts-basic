import { subDays, format } from 'date-fns';

const date = new Date(1998, 1, 28);
const rta = subDays(date, 30);
const formatted = format(rta, 'yyyy-MM-dd');
console.log("🚀 ~ file: 16-date-fns.ts ~ line 6 ~ formatted", formatted)
