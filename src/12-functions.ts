(() => {
  type Sizes = 'small' | 'medium' | 'large';
  function createProductToJson(
    title: string,
    createAt: Date,
    stock: number,
    size: Sizes
  ) {
    return {
      title,
      createAt,
      stock,
      size,
    };
  }

  const product = createProductToJson('Producto 1', new Date(), 12, 'small');
  console.log('🚀 ~ file: 12-functions.ts ~ line 18 ~ product', product);

  const createProductToJsonV2 = (
    title: string,
    createAt: Date,
    stock: number,
    size?: Sizes
  ) => {
    return {
      title,
      createAt,
      stock,
      size,
    };
  };

  const productV2 = createProductToJsonV2('Producto 1', new Date(), 12);
  console.log('🚀 ~ file: 12-functions.ts ~ line 33 ~ productV2', productV2);
})();
