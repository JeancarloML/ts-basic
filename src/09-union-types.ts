(() => {
  let userId: number | string;
  userId = 1234;
  userId = '1234';

  function greeting(myText: string | number) {
    if (typeof myText === 'string') {
      console.log(
        '🚀 ~ file: 09-union-types.ts ~ line 19 ~ myText',
        myText.toUpperCase()
      );
    } else {
      console.log(
        '🚀 ~ file: 09-union-types.ts ~ line 21 ~ myText',
        myText.toFixed(2)
      );
    }
  }
  greeting('Hola');
  greeting(1234.545);
})();
