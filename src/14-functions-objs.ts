(() => {
  const login = (data: { email: string; password: string }) => {
    console.log(data.email, data.password);
  };

  const data = {
    email: 'jean@gmail.com',
    password: '1234',
  };
  login(data);

  const products = [];

  const addProduct = (data: {
    title: string;
    createAt: Date;
    stock: number;
    size?: string;
  }) => {
    products.push(data);
  };

  addProduct({
    title: 'Producto 1',
    createAt: new Date(),
    stock: 12,
  });
  addProduct({
    title: 'Producto 2',
    createAt: new Date(),
    stock: 124,
    size: 'M',
  });

  // console.log('🚀 ~ file: 14-functions-objs.ts ~ line 33 ~ products', products);
})();
