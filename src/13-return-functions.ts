(() => {
  const calcTotal = (prices: number[]): string => {
    let total = 0;
    total = prices.reduce((e, i) => e + i);
    console.log(`El total es: ${total}`);
    return total.toString();
  };

  const printTotal = (prices: number[]): void => {
    console.log(`El total es: ${calcTotal(prices)}`);
  };

  const Nguardado = printTotal([12, 34, 31, 4, 134]);
})();
