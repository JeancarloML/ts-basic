(() => {
  let myDinamicVar: any;
  myDinamicVar = 'hola';
  myDinamicVar = 121212;
  myDinamicVar = true;
  myDinamicVar = {};

  myDinamicVar = 'Hola';
  const rta = (myDinamicVar as string).toUpperCase();
  console.log('🚀 ~ file: 08-any.ts ~ line 10 ~ rta', rta);

  myDinamicVar = 1234;
  const rta2 = <number>myDinamicVar.toFixed(2);
  console.log('🚀 ~ file: 08-any.ts ~ line 14 ~ rta2', rta2);
})();
