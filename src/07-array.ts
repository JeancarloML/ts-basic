(() => {
  let prices = [1, 2, 3, 4, 5, 'hola', true];
  prices.push(121212);
  console.log('🚀 ~ file: 07-array.ts ~ line 4 ~ prices', prices);

  let products: Array<number | string | boolean | Object> = ['hola', true];
  // let products: (number | string | boolean)[]= ['hola', true];
  products.push(121212);

  let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  numbers.map((n) => n * 2);
})();
