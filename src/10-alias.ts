(() => {
  type UserID = number | string | boolean;
  let userId: UserID;
  userId = 1234;
  userId = '1234';

  // LITERAL TYPES
  type Size = 'XS' | 'S' | 'M' | 'L' | 'XL' | 'XXL';
  let shirtSize: Size;
  shirtSize = 'M';
  shirtSize = 'XL';
  shirtSize = 'XXL';

  function greeting(myText: UserID, size: Size) {
    if (typeof myText === 'string') {
      console.log(
        '🚀 ~ file: 09-union-types.ts ~ line 19 ~ myText',
        myText.toUpperCase()
      );
    } else if (typeof myText === 'number') {
      console.log(
        '🚀 ~ file: 09-union-types.ts ~ line 21 ~ myText',
        myText.toFixed(2)
      );
    }
  }
})();
