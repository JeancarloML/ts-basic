(() => {
  /* let myNumber: number = undefined;
  let myString: string = null; */
  let myNull: null = null;
  let myUndefined: undefined = undefined;

  let myNumber: number | null = null;
  myNumber = 1234;

  let myString: string | null = null;
  myString = 'sdds';

  function hi(name: string | null) {
    let hello = 'hola';
    if (name) {
      hello += ` ${name}`;
      console.log(
        '🚀 ~ file: 11-undefined-null.ts ~ line 17 ~ hi ~ hello',
        hello
      );
    } else {
      hello += ' anonimo';
      console.log(
        '🚀 ~ file: 11-undefined-null.ts ~ line 21 ~ hi ~ hello',
        hello
      );
    }
  }

  hi('juan');
  hi(null);

  function hiv2(name: string | null) {
    let hello = 'Hola ';
    hello += name?.toLowerCase() ?? 'anonimo';
    console.log(
      '🚀 ~ file: 11-undefined-null.ts ~ line 33 ~ hiv2 ~ hello',
      hello
    );
  }

  hiv2('Juan');
  hiv2(null);
})();
