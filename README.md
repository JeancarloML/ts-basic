# Typescript

Para convertir un archivo .ts a .js, y establecer la version de ECMA, se debe ejecutar el siguiente comando:

```javascript
npx tsc src/demo.ts --target es6
```

Para convertir un archivo .ts a .js, y colocarlos en un directorio especifico, se debe ejecutar el siguiente comando:

```javascript
npx tsc src/*.ts --target es6 --outDir dist
```

Para console log

```javascript
Quokka: start on current file
```
